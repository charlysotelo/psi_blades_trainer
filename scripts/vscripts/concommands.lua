-- Create manager object
if CTrainerConCommands == nil then
  _G.CTrainerConCommands = class({})
end

-- Convar flags
FCVAR_CHEAT = bit.lshift(1, 14)    

function CTrainerConCommands:Activate()
  CTrainerConCommands:RegisterConCommands()
  CTrainerConCommands:RegisterConVars()
end

function CTrainerConCommands:RegisterConCommands()
  Convars:RegisterCommand("lua_execute", LuaExecute, "Excute string as lua code", FCVAR_CHEAT)
end

function CTrainerConCommands:RegisterConVars()
  Convars:RegisterConvar("psi_blades_width", "75", "Spill width for drawn box. Effective width is twice this.", 0)
  Convars:RegisterConvar("psi_blades_height", "200", "spill height for drawn box", 0)
  Convars:RegisterConvar("psi_blades_fps", "30", "how many times to update the trainer per second", 0)
  Convars:RegisterConvar("psi_blades_sphere_pad", "100", "Extra padding for the querying sphere", 0)
  
  Convars:RegisterConvar("psi_blades_draw_range", "1", "draw the attack range", 0)
  Convars:RegisterConvar("psi_blades_draw_helper", "1", "draw the psi blade helper", 0)

  Convars:RegisterConvar("psi_blades_level1_spill_range", "590", "how many times to update the trainer per second", 0)
  Convars:RegisterConvar("psi_blades_level2_spill_range", "630", "how many times to update the trainer per second", 0)
  Convars:RegisterConvar("psi_blades_level3_spill_range", "670", "how many times to update the trainer per second", 0)
  Convars:RegisterConvar("psi_blades_level4_spill_range", "710", "how many times to update the trainer per second", 0)

end

-- ConCommand Functions

LuaExecute = function(param1, param2)
  print("luaexecute got", param1, param2)
  f = loadstring(param2)
  if not pcall(f) then
    print ("Error executing lua code...")
  end
end