function RotateZ90Counter(vector)
  return Vector(-vector.y, vector.x, vector.z)
end

function RotateZ(vector, angle)
  return Vector(vector.x * math.cos(angle) - vector.y * math.sin(angle), vector.x * math.sin(angle) + vector.y * math.cos(angle), vector.z)
end

function Dot(vector1, vector2)
  return (vector1.x * vector2.x + vector1.y * vector2.y + vector1.z * vector2.z)
end

function IsPointIn2DBox(boxCenter, boxMax, boxMin, boxDirection, point)
  -- Idea: rotate everything to align boxDirection any axis, then simply check bounds
  angle = math.acos(Dot(boxDirection, Vector(1,0,0))) -- = the angle between them
  angle = (boxDirection.y > 0) and -angle or angle
  
  trans_point = point - boxCenter -- Bring boxCenter to origin  
  trans_rot_point = RotateZ(trans_point, angle) -- some precision lost here im assuming
  
  -- x direction
  if trans_rot_point.x > boxMax.x  or trans_rot_point.x < boxMin.x then
    return false
  end
  
  -- y direction
  if trans_rot_point.y > boxMax.y  or trans_rot_point.y < boxMin.y then
    return false
  end
  
  return true
end