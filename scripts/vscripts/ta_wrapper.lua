require("global_utils")
require("mathutils")

if CTaWrapper == nil then
  _G.CTaWrapper = class({})
end

function CTaWrapper:new(h)
  local newObj = {handle = h, hullRad = h:GetHullRadius()}
  self.__index = self
  return setmetatable(newObj, self)
end

function CTaWrapper:DrawAll(duration)
  if Convars:GetBool("psi_blades_draw_range") then
    self:DrawAttackRange(duration)
  end
  
  if Convars:GetBool("psi_blades_draw_helper") then
    self:DrawPsiBladesHelpers(duration)
  end
end

function CTaWrapper:DrawAttackRange(duration)
  local alpha = 20
  local ztest = true
  local drawColor = Vector(255, 0, 0)
  local playerPos = self:GetPosition()
  local attackRange = self:GetAttackRange()
  local radius = attackRange + self.hullRad
  DebugDrawCircle(playerPos, drawColor, alpha, radius, ztest, duration)
end

-- As of 6.88, Valve said the spill width is 75, when measured, its twice that
-- The range is indeed what the ingame skill says it is.
-- How the the targetting works:
-- checks for unit's origin to be inside the rectangular volume anchored at target1's origin.
function CTaWrapper:DrawPsiBladesHelpers(duration)

  -- Gather local vars
  local attackableUnits = self:GetAttackableUnits()
  local playerPos = self:GetPosition()
  local psiBladesSpillRange = self:GetPsiBladesSpillRange()
  local boxWidth = Convars:GetFloat("psi_blades_width")
  local boxHeight = Convars:GetFloat("psi_blades_height")
  
  -- Draw the psi blades helper for each attackable unit
  for idx, npc in pairs(attackableUnits) do
  
    -- Calculate box dimensions, position, and orientation
    local npcPos = npc:GetAbsOrigin()
    local boxDirection = npcPos - playerPos
    boxDirection.z = 0 -- We are doing 2d vector math here
    boxDirection = boxDirection:Normalized() -- some precision lost here. How do i compensate?
    local boxCenter = npcPos + ((psiBladesSpillRange/2) * boxDirection)
    local boxMax = Vector(psiBladesSpillRange/2, boxWidth, boxHeight)
    local boxMin = Vector(-psiBladesSpillRange/2, -boxWidth, 0)
    
    -- Coarse test for units in the rectangular volue
    local units = FindUnitsInLine(
      self.handle:GetTeam(),
      npcPos, 
      npcPos + (boxDirection * psiBladesSpillRange), 
      self.handle, 
      boxWidth, 
      DOTA_UNIT_TARGET_TEAM_ENEMY,
      DOTA_UNIT_TARGET_HERO,
      0)
    
    -- Ensure the units are indeed in the volume. If this is the case, change the color of the box
    local color = Vector(255, 0, 0)
    for _,unit in pairs(units) do
      if unit:entindex() ~= npc:entindex() and IsPointIn2DBox(boxCenter, boxMax, boxMin, boxDirection, unit:GetAbsOrigin()) then
        color = Vector(0, 255, 0)
      end
    end
    DebugDrawBoxDirection(boxCenter, boxMax, boxMin, boxDirection, color, 20, duration)
  end
end

function CTaWrapper:GetPosition()
  return self.handle:GetAbsOrigin()
end

function CTaWrapper:GetAttackRange()
  return self.handle:GetAttackRange()
end

function CTaWrapper:GetPsiBladesSpillRange()

  local psiBladesAbility = self.handle:GetAbilityByIndex(2)
  local psiBladesLevel = psiBladesAbility:GetLevel()
  
  if psiBladesLevel == 0 then
    return 0
  end
  
  if psiBladesLevel == 1 then
    return Convars:GetFloat("psi_blades_level1_spill_range")
  end
  
  if psiBladesLevel == 2 then
    return Convars:GetFloat("psi_blades_level2_spill_range")
  end
  
  if psiBladesLevel == 3 then
    return Convars:GetFloat("psi_blades_level3_spill_range")
  end
  
  if psiBladesLevel == 4 then
    return Convars:GetFloat("psi_blades_level4_spill_range")
  end
end

function CTaWrapper:CanHit(hEnt)
  return hEnt ~= nil and hEnt ~= self.handle and IsNPC(hEnt) and not hEnt:IsBuilding() and hEnt:IsAlive() and hEnt:GetName() ~= "Announcer" 
end

function CTaWrapper:InAttackRange(hEnt)
  local delta2D = (self.handle:GetAbsOrigin() - hEnt:GetAbsOrigin()):Length2D() - (self.hullRad + hEnt:GetHullRadius())
  local attackRange = self:GetAttackRange()
  return delta2D <= attackRange
end

function CTaWrapper:GetAttackableUnits()
  
  -- Gather local vars
  local attackerPos = self.handle:GetAbsOrigin()
  local attackerTeam = self.handle:GetTeamNumber()
  local attackRange = self:GetAttackRange()
  local spherePad = Convars:GetFloat("psi_blades_sphere_pad")
  local queryRange = attackRange + 2*self.hullRad + spherePad
  local nearbyEntities = Entities:FindAllInSphere(attackerPos, queryRange)
  local attackableUnits = {}
  
  -- Iteraterate through nearby Entities
  for idx, ent in pairs(nearbyEntities) do
    if self:CanHit(ent) then
      local entTeam = ent:GetTeamNumber()
      local entHealth = ent:GetHealthPercent()
    
      -- Don't forget friendly creeps!
      if entTeam ~= attackerTeam or entHealth < 50 then
        if self:InAttackRange(ent) then
          table.insert(attackableUnits, ent)
        end
      end
    end
  end
  
  return attackableUnits
end