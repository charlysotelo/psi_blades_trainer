require("concommands")
require("mathutils")
require("ta_wrapper")
require("ta_trainer")

if CPsiBladesTrainer == nil then
  _G.CPsiBladesTrainer = class({})
end

function Precache( context )
  --[[
    Precache things we know we'll use.  Possible file types include (but not limited to):
      PrecacheResource( "model", "*.vmdl", context )
      PrecacheResource( "soundfile", "*.vsndevts", context )
      PrecacheResource( "particle", "*.vpcf", context )
      PrecacheResource( "particle_folder", "particles/folder", context )
  ]]
end

function Activate()
  CPsiBladesTrainer:Activate()
  CTrainerConCommands:Activate()
end