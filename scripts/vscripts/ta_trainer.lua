if CPsiBladesTrainer == nil then
  _G.CPsiBladesTrainer = class({})
end

function CPsiBladesTrainer:Activate()
  print( "ActivateTrainer called" )
  self.taTable = {}
  ListenToGameEvent("npc_spawned", Dynamic_Wrap(CPsiBladesTrainer, "NpcSpawned"), self)
  GameRules:GetGameModeEntity():SetThink( "Draw", self, "GlobalThink", 2 )
end

function CPsiBladesTrainer:NpcSpawned(eventInfo)
  entIndex = eventInfo.entindex
  if not self.taTable[entIndex]  then
    handle = EntIndexToHScript(entIndex)
    if handle:GetName() == "npc_dota_hero_templar_assassin" then
      self.taTable[entIndex] = CTaWrapper:new(handle)
    end
  end
end

function CPsiBladesTrainer:Draw()
  local psi_blades_fps = Convars:GetFloat("psi_blades_fps")
  for entIndex, ta in pairs(self.taTable) do
    ta:DrawAll(1/psi_blades_fps)
  end
  return 1/psi_blades_fps
end