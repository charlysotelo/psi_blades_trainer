function IsNPC( hEnt )
  if hEnt.ManageModelChanges then
    return true
  end
  return false
end

function PrintSelectedHeroAbsOrigin(iPlayerId)
  print(PlayerResource:GetSelectedHeroEntity(iPlayerId):GetAbsOrigin())
end

function SetSelectedHeroAbsOrigin(iPlayerId, x, y, z)
  PlayerResource:GetSelectedHeroEntity(iPlayerId):SetAbsOrigin(Vector(x, y, z))
end